import React from 'react';
import './App.scss';
import { stringList } from './models/strings';
import { LangSelect } from './component/langSelect';
import { BackgroundMap, GoogleMapsScript } from './component/backgroundMap';
import { LocationSearch } from './component/locationSearch';
import { WeatherDisplay } from './component/weatherDisplay';
import { getGeoLocation } from './actions/getGeolocation';
import { store, Store } from './store';
import { EventsEnum } from './models/eventsEnum';
import { Favorites } from './component/favorites';
import { UserFavoriteList } from './component/userFavoriteList';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";
import { getColor } from './actions/getColors';
import { Welcome } from './component/welcome';

getGeoLocation.then(
  (d) => {
    store.dispatchEvent(EventsEnum.GEO_LOCATION_RECEIVED, d);

  },
  (err) => {
    store.dispatchEvent(EventsEnum.GEO_LOCATION_FAILED, err);
  }
);


class App extends React.Component<{}, Partial<Store>> {

  constructor(props) {
    super(props);
    this.state = {}
  }
  componentDidMount() {
    document.addEventListener(EventsEnum.WEATHER_DATA_LOADED, () => {
      if (store.weatherDataLoaded)
        this.setState({ color: getColor(store.weatherDataLoaded.currently.temperature) });
    });

  }
  //<UserFavoriteList/>
  render() {
    return <Router>
      <GoogleMapsScript />
      <BackgroundMap color={this.state.color} />
      
      <article className="l-layer-up h-full b-mainPanel">
        <header className="b-mainPanel__header">
          <Favorites color={this.state.color} />
        </header>
        <main className="l-overflow-auto b-mainPanel__content">
          <WeatherDisplay color={this.state.color}/>
        </main>
      </article>
    </Router>
  }
}
export default App;

