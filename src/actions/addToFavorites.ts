import { store } from '../store';
import { EventsEnum } from '../models/eventsEnum';
import { ILocation } from '../models/location';

export const addToFaforites = (item: ILocation)=>{
    if(store.favoriteList.find(e=>e.name === item.name)) return;
    store.favoriteList.push( item );
    store.dispatchEvent(EventsEnum.ADD_TO_FAVORITES, store.favoriteList);
}