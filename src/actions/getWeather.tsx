import { IWeather } from "../models/weather";
import { rejects } from "assert";

export const getWeather = (lat: number, lon: number): Promise<IWeather> => {
    if(! lat|| ! lon) return new Promise((resolve)=>{
        resolve(undefined);
    })
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {    
                resolve(JSON.parse(this.responseText));
            }
        });

        xhr.open("GET", `https://dark-sky.p.rapidapi.com/${lat},${lon}?lang=en&units=si`);
        xhr.setRequestHeader("x-rapidapi-key", "dd589b9334mshd00f7018febc86fp199eaejsn8f7afb446115");

        xhr.send();
    });
}
