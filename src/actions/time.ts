export const formatDSTime = (time: number): Date =>{
    return new Date( +(time + '000'))
}

export const formattedTime = ( time: number ): string => {
    const timeFormatted = formatDSTime(time);
    const months = ["01", "02", "03", "04", "05", "06", "07", "08", "9", "10", "11", "12"];
    const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Satarday"];
    return `
        ${timeFormatted.getDate()}.${months[timeFormatted.getMonth()]}.${timeFormatted.getFullYear()},
        ${days[timeFormatted.getDay()]}
    `
}

export const zeroPad = ( val: number | string ): string => {
    const _val:string = '' + val;
    console.log(_val.length);
    if( _val.length <= 1 ){
        return '0' + val;
    } else {
        return val as string;
    }
}
export const formattedHour = (time: number): string => {
    const timeFormatted = formatDSTime(time);
    return `${zeroPad(timeFormatted.getHours())}:${zeroPad(timeFormatted.getMinutes())}`
}