export const getGeoLocation = new Promise<Position>((resolve, reject) => {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(resolve);
    } else {
        reject('Geolocation failed');
    }
})