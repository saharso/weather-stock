const colors = [
    '#e1f5fe',
    '#e3f2fd',
    '#b3e5fc',
    '#81d4fa',
    '#4fc3f7',
    '#4dd0e1',
    '#a5d6a7',
    '#ffd54f',
    '#f57c00',
    '#d84315',
];

const style = document.createElement('style');


let colorCssVars = '';

const colorSchemeMap = {};
let start = -50;
let max: number = 0;
colors.forEach((color, index)=>{
  let range1 = start + (index * 10);
  let range2 = range1 + 9.999999;
  max = range2;
  colorCssVars += `--c-${range1}-${Math.ceil(range2)}: ${color};\n`;
  colorSchemeMap[color] = [range1,range2];
});

style.innerHTML =`
  html {
    ${colorCssVars}
  }
`
;
document.head.appendChild(style);


export const getColor = (n: number)=>{
  if( n < start ){
    return colors[0];
  } else if ( n > max){
    return colors[colors.length - 1];
  } else {
    for( const key in colorSchemeMap ){
      if( n > colorSchemeMap[key][0] && n <= colorSchemeMap[key][1] ){
        return key;
      } 
    }
  }
}


// source : https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=D84315