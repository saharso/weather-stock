import { EventsEnum } from "./models/eventsEnum";
import { ILocation } from "./models/location";
import { WeatherDisplay } from "./component/weatherDisplay";
import { IWeather } from "./models/weather";
import { ILocatinoFull } from "./models/locationFull";
 export class Store {
    lang: string;
    googleMapsApiKey: string = 'AIzaSyALpVaI0naxLKBEPC1xgcg-6xLJgAOcep0';
    googleMapScriptLoaded: boolean = false;
    backgroundMap: any;
    selectedLocation: ILocation;
    weatherDataLoaded: IWeather;
    geoLocationReceived: Position;
    color: string;
    fullLocation: ILocatinoFull;
    favoriteList: ILocation[] = [];

    favoriteLocations: ILocation[] = [
        {
            name: "London",
            lat: 51.5073509,
            lon: -0.1277583,
        },
        {
            name: "Paris",            
            lat: 48.85661400000001,
            lon: 2.3522219,
        },
        {
            name: "Berlin",
            lat: 52.52000659999999,
            lon: 13.404954,
        },
        {
            name: "Zürich",            
            lat: 47.3768866,
            lon: 8.541694,
        },
        {
            name: "Rome",        
            lat: 41.90278349999999,
            lon: 12.4963655,
        },
        {
            lat: 40.4167754,
            lon: -3.7037902,
            name: "Madrid",
        },
    ];
    locationRoutes = [
        {
            path: '/',
            component: WeatherDisplay,
        },
    ];
    dispatchEvent(eventName: EventsEnum, data: any){
        const event = new CustomEvent(eventName,{detail: data});
        this[eventName] = data;
        document.dispatchEvent( event );
    }
    removeCustomEvent(eventName: EventsEnum, handler){
        document.removeEventListener(eventName, handler, false);
    }
}

export const store = new Store();
