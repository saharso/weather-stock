
import React from 'react';
import { store, Store } from '../store';
import { EventsEnum } from '../models/eventsEnum';
import { IWeather } from '../models/weather';
import { getColor } from '../actions/getColors';
import { getGeoLocation } from '../actions/getGeolocation';

export class GoogleMapsScript extends React.Component<Partial<Store>,{}> {

  zoom: number = 8;
  componentDidMount(){
    const script = document.createElement("script");

    script.src = `https://maps.googleapis.com/maps/api/js?key=${store.googleMapsApiKey}&libraries=places`;
    script.async = true;

    script.addEventListener('load',()=>{
      requestAnimationFrame(()=>{
        store.dispatchEvent(EventsEnum.GOOGLE_MAP_SCRIPT_LOADED, true);
      });
    });
    document.body.appendChild(script);
  }
  render(){
    return null;
  }
}
interface IBackgroundMap {
  color: string;
}
export class BackgroundMap extends React.Component<{color:string}, IBackgroundMap> {
  map: any;
  zoom: number = 11;
  center = {lat: 48.860001, lng: 2.350000};

  componentDidMount(){
    this.initByGeoLocation();
  }
  
  onGoogleScriptLoad () {
      if(store.googleMapScriptLoaded){
        this.initGoogleMap();
      } else {
        document.addEventListener(EventsEnum.GOOGLE_MAP_SCRIPT_LOADED, this.initGoogleMap.bind(this));
      }

      document.addEventListener(EventsEnum.LOCATION_SELECTED,()=>{
        this.goTo(store[EventsEnum.LOCATION_SELECTED].lat, store[EventsEnum.LOCATION_SELECTED].lon)
      });

  }

  initByGeoLocation(){
    const setCoors = ()=>{
      const location = store[EventsEnum.GEO_LOCATION_RECEIVED] as Position;
      this.center.lat = location.coords.latitude;
      this.center.lng = location.coords.longitude;
      this.onGoogleScriptLoad();
    }
    if(store[EventsEnum.GEO_LOCATION_RECEIVED]){
      setCoors();
    } else {
      document.addEventListener(EventsEnum.GEO_LOCATION_RECEIVED,(e)=>{
        setCoors();
      });
      this.onGoogleScriptLoad();
    }

  }

  initGoogleMap(){
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: this.center,
      zoom: this.zoom,
      disableDefaultUI: true,
    });
    store.backgroundMap = this.map;
  }

  animateZoom(from: number, to: number, onAfterZoom?: Function ){
    let _from = from, _to = to;
    const indDec = (n:number): number=>{
      if(_from < _to ) return n+=1;
      else return n-=1;
    }
    const animate = ()=>{
      if(_from !== _to){
        setTimeout(()=>{
          _from = indDec(_from);
          this.map.setZoom(_from);
          animate();
        },50)
      } else {//tilesloaded
        this.map.addListener('idle', ()=>{
          requestAnimationFrame(()=>{
            onAfterZoom && onAfterZoom();
            google.maps.event.clearListeners(this.map, 'idle');
            google.maps.event.clearListeners(this.map, 'tilesloaded');
          });
        });
      }
    }
    animate();
  }
  
  goTo(lat,lon){
    this.animateZoom( this.zoom, this.zoom - 4, ()=>{
      var latLng = new google.maps.LatLng(lat, lon);
      this.map.panTo(latLng);
      this.animateZoom( this.zoom - 4, this.zoom );
    } );

  }

  render() {
    return (

    <div className="l-overlay b-backgroundMap">
      <div id="map" className="b-backgroundMap__map"></div>
      <div 
        className="l-overlay b-backgroundMap__colorOverlay"
      ></div>
    </div>
    )


  }
}
 
  //apiKey: (store.googleMapsApiKey)
