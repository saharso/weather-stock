import React from 'react';
import { store } from '../store';
import { ILocation } from '../models/location';
import { EventsEnum } from '../models/eventsEnum';

export class UserFavoriteList extends React.Component<{},{favoriteList:ILocation[]}> {

    constructor(props){
        super(props);
        this.state = {
            favoriteList: store.favoriteList,
        }
    }

    componentDidMount(){
        document.addEventListener(EventsEnum.ADD_TO_FAVORITES,()=>{
            this.setState({
                favoriteList: store.favoriteList,
            })
        })
    }
    render(){
        return <ul>

            {this.state.favoriteList.map((item,index)=>{
                return <li key={index}>{item.name}</li>
            })}

        </ul>
    }
}