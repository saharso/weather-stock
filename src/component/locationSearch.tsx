import React from 'react';
import { EventsEnum } from '../models/eventsEnum';
import { store } from '../store';
import { ILocation } from '../models/location';

export class LocationSearch extends React.Component {

    componentDidMount(){
        if(store.googleMapScriptLoaded){
            requestAnimationFrame(this.initAutoComplete.bind(this));
        } else {
            document.addEventListener(EventsEnum.GOOGLE_MAP_SCRIPT_LOADED,()=>{
                this.initAutoComplete.call(this);
            });
        }    
    }
    initAutoComplete(){

        const input: HTMLElement = document.getElementById('searchTextField');
        var autocomplete = new google.maps.places.Autocomplete(input as HTMLInputElement);          
        google.maps.event.addListener(autocomplete, 'place_changed', function (d) {
            if( ! autocomplete.getPlace().geometry ) return;
            const selectedLocation: ILocation = {
                name: autocomplete.getPlace().name,
                lat: + autocomplete.getPlace().geometry.location.lat(),
                lon: + autocomplete.getPlace().geometry.location.lng(),
            }
            store.dispatchEvent( EventsEnum.LOCATION_SELECTED, selectedLocation );
        });
        input.focus();
    }

    render(){
        return <div className="t-box l-align-x l-align-top b-locationSearch">

            <div className="b-locationSearch__searchWrap">
                <input className="b-locationSearch__search" id="searchTextField" type="text" />
            </div>

        </div>
    }
}