import React from 'react';
import { store, Store } from '../store';
import {
    Link
  } from "react-router-dom";
import { EventsEnum } from '../models/eventsEnum';

export class Favorites extends React.Component<{color: string},{}> {

    getLocationWeather(item){
        store.dispatchEvent(EventsEnum.LOCATION_SELECTED,item)
    }

    render(){
        return <nav style={{backgroundColor: this.props.color}} className="l-flex b-favorites">
            
            {
                store.favoriteLocations.map( (item, index)=>{
                    return <button onClick={this.getLocationWeather.bind(this, item)} className="b-favorites__item" key={index}>
                        {item.name}
                    </button>
                })
            }
        </nav>
    }
}