
import React from 'react';
import { store } from '../store';
import { EventsEnum } from '../models/eventsEnum';
import { getWeather } from '../actions/getWeather';
import { IWeather, DailyData, Datum } from '../models/weather'
import { ILocation, ILocationStringy } from '../models/location';
import { Welcome } from './welcome'; 
import { LocationSearch } from './locationSearch';
import { addToFaforites } from '../actions/addToFavorites';
import { formatDSTime, formattedTime, formattedHour } from '../actions/time';

export class WeatherDisplay extends React.Component<{color:string},Partial<{weather:IWeather,loc:ILocation}>> {

    constructor(props){
        super(props);
        this.state = {
            weather: null,
            loc: null,
        }
    }
    componentDidMount(){
        document.addEventListener(EventsEnum.LOCATION_SELECTED,(e: CustomEvent)=>{
            const selectedLocation: ILocation = e.detail;
            if(!selectedLocation)return;
            this.setupData(selectedLocation);
        });
    }
    
    private setupData(selectedLocation: ILocation){
        if ( ! selectedLocation ) return;
        this.setState({
            loc: selectedLocation,
        });
        getWeather( selectedLocation.lat, selectedLocation.lon ).then((data)=>{
            this.setState( {
                weather: data,
            } );
            store.dispatchEvent( EventsEnum.WEATHER_DATA_LOADED, data as IWeather);
        });
    }

    addToFavorites(){
        addToFaforites(this.state.loc);
    }

    render(){
        if( this.state.weather){
            if(!this.state.weather.currently){
                return <div>
                     <h1 className="b-weatherDisplay__welcomeMessage">
                        Sorry, location not found.
                     </h1>
                </div>
            }
            const icon = this.state.weather.currently.icon;
            const temperature = this.state.weather.currently.temperature;
            const summary = this.state.weather.daily.summary;
            const daily: DailyData[] = this.state.weather.daily.data;
            console.log(this.state.weather);
            // todo hourly
            
            return <section 
                style={{color: this.props.color}} 
                className="l-overflow-auto l-flex-column b-weatherDisplay"
            >

                <div className="t-box l-cellSpread-y l-overflow-auto b-weatherDisplay__content">
                    <header className=" t-row l-align-bottom b-weatherDisplay__header">
                        <h1 className="t-row-0 fw-thin l-cellSpread-x l-marginEnd b-weatherDisplay__locationName">
                            {this.state.loc.name}
                        </h1>
                        <div className="l-align-y">
                            <LocationSearch/>
                            <button onClick={this.addToFavorites.bind(this)} className="t-marginStart">
                                <i className="fas fa-heart"></i>
                            </button>
                        </div>
                    </header>

                    <div className="t-row l-align-y">
                        <figure className="t-marginEnd b-weatherDisplay__icon">
                            <img src={require(`../assets/icons/${icon}.svg`)} alt={icon}/>
                        </figure>
                        <div className="b-weatherDisplay__temperature">
                            {temperature}&deg;
                        </div>
                    </div>

                    <p className="t-row-0 b-weatherDisplay__summary">
                        {summary}
                    </p>

                </div>

                <ul className="norm-ul w-full t-box b-weatherDisplay__dailyList">
                    {daily.map(( day: DailyData, index: number)=>{
                    if( index >= 7 ) return undefined;
                    return <li className="ta-center l-flex-column b-weatherDisplay__dailyItem" key={index}>
                        <figure className="l-auto t-row b-weatherDisplay__icon b-weatherDisplay__icon--smaller">
                            <img src={require(`../assets/icons/${day.icon}.svg`)} alt={icon}/>
                        </figure>
                        <h3 className="b-weatherDisplay__title">
                            {day.temperatureLow}&deg; - {day.temperatureHigh}&deg;
                        </h3>
                        <p className="l-cellSpread">
                            {day.summary}
                        </p>
                        <time className="b-weatherDisplay__time">
                            { formattedTime(day.time) }
                        </time>
                    </li>
                    })}
                </ul>

            </section>
        } else {
            return <Welcome />
        }
    }
}