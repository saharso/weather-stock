import React from 'react';
import { LocationSearch } from './locationSearch';
export class Welcome extends React.Component {
    render (){
        return <div className="h-full l-flex-column l-centralize-2d">
        <h1 className="b-weatherDisplay__welcomeMessage">
            <span className="b-weatherDisplay__welcomeMessage__big">
                WELCOME!
            </span>
            <span className="b-weatherDisplay__welcomeMessage__small">
                This is a weather app.
            </span>
            <strong className="b-weatherDisplay__welcomeMessage__small">
                Choose a location to get started
            </strong>
        </h1>
        <LocationSearch />
    </div>
    }
}