import React from 'react';
import {store} from '../store'
import { EventsEnum } from '../models/eventsEnum';
import { LangList } from '../models/strings';

export class LangSelect extends React.Component<{},{label:string}> {
    langList = new LangList();
    constructor(props){
      super(props);
      this.state = {
        label: props.label,
      }
    }
    
    componentDidMount(){
      requestAnimationFrame(()=>{
        this.onLangChange( 'he' );
      })
    }
    
    onLangChange(e: any){
      let key: string;
      if( typeof e === 'string'){
        key = e;
      } else {
        key = e.target.value;
      }
      store.lang = key;
      store.dispatchEvent(EventsEnum.LANG_CHANGES, key);
    }
  
    render(){
      
      const optionList = Object.keys( this.langList ).map( ( key, i ) => <option key={i} value={key}>{this.langList[key]}</option>)
      return <div>
          <label>{this.state.label}</label>
          <select onChange={this.onLangChange}>{optionList}</select>
        </div>
      
    }
    
  }