export interface ILocation {
    name: string;
    lat: number;
    lon: number;
}

export interface ILocationStringy {
    name: string;
    lat: string;
    lon: string;
}

