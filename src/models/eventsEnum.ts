export enum EventsEnum {
    LANG_CHANGES = 'langChanged',
    GOOGLE_MAP_SCRIPT_LOADED = 'googleMapScriptLoaded',
    LOCATION_SELECTED = 'selectedLocation',
    WEATHER_DATA_LOADED = 'weatherDataLoaded',
    GEO_LOCATION_RECEIVED = 'geoLocationReceived',
    GEO_LOCATION_FAILED = 'geoLocationFailed',
    FULL_LOCATION = 'fullLocation',
    ADD_TO_FAVORITES = 'addToFavorites',
}