export class LangList {
    he: string = 'Hebrew';
    en: string = 'English';
    es: string = 'Spanish';
}
class Strings {
    HELLO_WORLD: LangList = {
        he: 'שלום עולם',
        en: 'Hello World',
        es: 'Hola Mundo',
    };
    GOODBYE_WORLD: LangList = {
        he: 'להתראות עולם',
        en: 'Goodbye World',
        es: 'Adiós Mundo',
    };
    HI_FRIEND: LangList = {
        he: 'שלום, ___, מה שלומך?',
        en: 'Hi, ___, how are you?',
        es: 'Hola, ___, como estas?',
    };
    VERY_COMPLEX_STRING: LangList = {
        he: 'השעה',
        en: 'This, my good friend, ___, is a very complex ___ to think about.',
        es: 'Son las',
    };
    ITS: LangList = {
        he: 'השעה',
        en: 'I\'s',
        es: 'Son las',
    };
    setStr( prop, addition: string[] ){
        const langList: LangList = new LangList();
        Object.keys(prop).forEach((key)=>{
            const r = prop[key].split('___');
            let str: string = '';
            r.forEach((txt,index)=>{
                str += txt + ( addition[index] || '' );
            });
            langList[key] = str;
        });
        return langList;
    }
}
export const stringList: Strings = new Strings();