
export interface AddressComponent {
    long_name: string;
    short_name: string;
    types: string[];
}

export interface Location {
    lat: number;
    lng: number;
}

export interface Viewport {
    south: number;
    west: number;
    north: number;
    east: number;
}

export interface Geometry {
    location: Location;
    viewport: Viewport;
}

export interface Photo {
    height: number;
    html_attributions: string[];
    width: number;
}

export interface ILocatinoFull {
    address_components: AddressComponent[];
    adr_address: string;
    formatted_address: string;
    geometry: Geometry;
    icon: string;
    id: string;
    name: string;
    photos: Photo[];
    place_id: string;
    reference: string;
    scope: string;
    types: string[];
    url: string;
    utc_offset: number;
    html_attributions: any[];
    utc_offset_minutes: number;
}



